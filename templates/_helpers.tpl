{{/* Chart name */}}
{{- define "MyAppCtx.chartName" -}}
{{- default .Chart.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Chart full name */}}
{{- define "MyAppCtx.fullname" }}
{{- $name := default .Chart.Name .Values.name -}}
{{- printf "%s" $name }}
{{- end }}

{{/* API */}}
{{- define "MyAppCtx.api" }}
{{- $name := default .Chart.Name .Values.name -}}
{{- printf "%s-api" $name}}
{{- end }}

{{/* FRONT */}}
{{- define "MyAppCtx.front" }}
{{- $name := default .Chart.Name .Values.name -}}
{{- printf "%s-front" $name}}
{{- end }}

{{/* DB */}}
{{- define "MyAppCtx.pg" }}
{{- $name := default .Chart.Name .Values.name -}}
{{- printf "%s-pg" $name}}
{{- end }}


{{/* Application image tag - We select by default the Chart appVersion or an override in values */}}
{{- define "MyAppCtx.imageTag" }}
{{- $name := default .Chart.AppVersion .Values.image.tag }}
{{- printf "%s" $name }}
{{- end }}


{{/* Common labels */}}
{{- define "MyAppCtx.labels" -}}
helm.sh/chart: {{ include "MyAppCtx.chart" . }} 
{{ include "MyAppCtx.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "MyAppCtx.selectorLabels" -}}
app.kubernetes.io/name: {{ include "MyAppCtx.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}